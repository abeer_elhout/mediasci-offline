@extends('front.layout')
@section('title')
<title>{{$post_data['title']}}</title>
@endsection
@section('header')
  <div class="internal-page">
  <div class="wrapper">
@endsection
@section('content')
  </div>
        <div class="internal-container">
            <div class="row">
                <div class="wrapper">
                    <aside class="col-md-3">
                        <p>{{$post_data['title']}}</p>
                    </aside>
                    <div class="col-md-9 main-text">
                        <div class="row">
                            @foreach(array_reverse($img_array) as $key => $value)
                              <div class="col-sm-6 col-md-4 room-container">
                                  <a href="{{url('uploads/'.$value->media_path)}}" data-title="1" data-lightbox="floors"> <img class="room img-thumbnail" src="{{url('uploads/'.$value->media_path)}}" alt=""></a>
                              </div>
                            @endforeach
                            <!-- <div class="col-sm-6 col-md-4 room-container">
                                <a href="images/room2.jpg" data-title="2" data-lightbox="floors"> <img class="room img-thumbnail" src="images/room2.jpg" alt=""></a>
                            </div>
                            <div class="col-sm-6 col-md-4 room-container">
                                <a href="images/room3.jpg" data-title="3" data-lightbox="floors"><img class="room img-thumbnail" src="images/room3.jpg" alt=""></a>
                            </div>
                            <div class="col-sm-6 col-md-4 room-container">
                                <a href="images/room4.jpg" data-title="4" data-lightbox="floors"> <img class="room img-thumbnail" src="images/room4.jpg" alt=""></a>
                            </div>
                            <div class="col-sm-6 col-md-4 room-container">
                                <a href="images/room5.jpg" data-title="5" data-lightbox="floors"><img class="room img-thumbnail" src="images/room5.jpg" alt=""></a>
                            </div>
                            <div class="col-sm-6 col-md-4 room-container">
                                <a href="images/room6.jpg" data-title="6" data-lightbox="floors"><img class="room img-thumbnail" src="images/room6.jpg" alt=""></a>
                            </div> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <p id="para">{{$post_data->long_description}}</p>
        </div>
</div>
<script>
    $(function(){
      $('.main-text').slimScroll({
           height: '320px',
           alwaysVisible: true
      });
    });
</script>
@stop
