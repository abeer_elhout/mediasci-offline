<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="javascripts/jquery.js"></script>
    <script src="javascripts/bootstrap.js"></script>
    <script src="javascripts/slimscroll.min.js"></script>
    <script src="javascripts/lightbox.js"></script>
    <script src="javascripts/main.js"></script>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="stylesheets/styles.css">
    <link rel="stylesheet" href="stylesheets/about.css">
    <link rel="stylesheet" href="stylesheets/internal.css">
    <link rel="stylesheet" href="stylesheets/lightbox.css">
    <link rel="stylesheet" href="stylesheets/contact.css">
    @yield('title')
    <title>Index</title>
</head>

<body>
  @yield('header')
    <nav class="navbar navbar-default">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{url('/index')}}">
            <img src="images/logo-1.png" alt="Media-Sci">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul  class="nav navbar-nav">
          <li><a href="{{url('/about-us')}}">About Us</a></li>
          <li><a href="{{url('/exhibitions')}}">Exhibitions</a></li>
          <li><a href="{{url('/internal-designing')}}">Internal Designing</a></li>
          <li><a href="{{url('/concept-development')}}">Concept Development</a></li>
          <li><a href="{{url('/branding')}}">Branding</a></li>
          <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
        </ul>
        <ul  class="nav navbar-nav navbar-right">
          <li>
            <ul class="soc">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li class="last-social">
                  <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </li>
            </ul>
          </li>
        </ul>
        <input class="pull-right search" type="text" placeholder="search">
      </div>
  </nav>
            @yield('content')
        <footer>
            <p><span>All rights reserved </span>@ mediasci</p>
        </footer>

</body>

</html>
