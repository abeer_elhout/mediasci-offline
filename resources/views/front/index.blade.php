@extends('front.indexlayout')
@section('title')
<title>Media SCI</title>
@endsection
@section('header')
<a href="{{url('/index')}}"><img width="150" height="100" src="images/logo-offline.jpg" alt="Media-sci-logo"></a>
<nav class="navbar navbar-default navhome">
@endsection
@section('content')
        <div class="transparent"></div>
        <div class="features">
            <div class="wrapper">
                <div class="row">
                  @foreach($posts as $key => $post)
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="feature-item">
                            <img class="main-img" src="images/exhib.jpg" alt="Exhibitions">
                            <div class="<?php if($key==0){echo "first";}elseif($key==1){echo "second";}elseif($key==2){echo "third";}else{echo "fourth";} ?>">
                                <h2>{{$post->title}}</h2>
                                <p>{{$post->short_description}}</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div>
                  @endforeach
                    <!-- <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="feature-item">
                            <img class="main-img" src="images/internal.jpg" alt="Internal Designing">
                            <div class="second">
                                <h2>Internal Designing</h2>
                                <p>Lorem ipsum dolor sit amet, fugit vituperata no ius, soleat acc</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="feature-item">
                            <img class="main-img" src="images/concept.jpg" alt="Concept Development">
                            <div class="third">
                                <h2>Concept Development</h2>
                                <p>Lorem ipsum dolor sit amet, fugit vituperata no ius, soleat acc</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="feature-item">
                            <img class="main-img" src="images/branding.jpg" alt="Branding">
                            <div class="fourth">
                                <h2>Branding</h2>
                                <p>Lorem ipsum dolor sit amet, fugit vituperata no ius, soleat acc</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
@stop
