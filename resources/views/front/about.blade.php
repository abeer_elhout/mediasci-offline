@extends('front.layout')
@section('title')
<title>{{$about_data->name}}</title>
@endsection
@section('header')
  <div class="about-page">
  <div class="wrapper">
@endsection
@section('content')
<body>

        </div>
        <div class="about-container">
            <div class="row">
                <div class="wrapper">
                    <aside class="col-md-2">
                        <p>{{$about_data->name}}</p>
                    </aside>
                    <div class="col-md-10 main-text">
                        <p>{{$about_data->description}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="features">
            <div class="wrapper">
                <div class="row">
                  @foreach($posts as $key => $post)
                    <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="feature-item">
                            <img class="main-img" src="images/exhib.jpg" alt="Exhibitions">
                            <div class="<?php if($key==0){echo "first";}elseif($key==1){echo "second";}elseif($key==2){echo "third";}else{echo "fourth";} ?>">
                                <h2>{{$post->title}}</h2>
                                <p>{{$post->short_description}}</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="feature-item">
                            <img class="main-img" src="images/internal.jpg" alt="Internal Designing">
                            <div class="second">
                                <h2>Internal Designing</h2>
                                <p>Lorem ipsum dolor sit amet, fugit vituperata no ius, soleat acc</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="feature-item">
                            <img class="main-img" src="images/concept.jpg" alt="Concept Development">
                            <div class="third">
                                <h2>Concept Development</h2>
                                <p>Lorem ipsum dolor sit amet, fugit vituperata no ius, soleat acc</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="feature-item">
                            <img class="main-img" src="images/branding.jpg" alt="Branding">
                            <div class="fourth">
                                <h2>Branding</h2>
                                <p>Lorem ipsum dolor sit amet, fugit vituperata no ius, soleat acc</p>
                                <img class="forma" src="images/forma.png" alt="">
                            </div>
                        </div>
                    </div> -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div>
                            <p id="para">{{$about_data->prief}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
