@extends('front.layout')
@section('title')
<title>Contact Us</title>
@endsection
@section('header')
  <div class="contact-page">
  <div class="wrapper">
@endsection
@section('content')
</div>
        <div class="map-container">
            <div class="row">
                <div class="wrapper">
                    <aside class="col-md-2">
                        <p>Contact Us</p>
                    </aside>
                    <div class="col-md-10" id="map"></div>
                </div>
            </div>
        </div>

        <div>
            <form method="post" id="contact-form">
                <div class="alert alert-success" hidden id="successDiv"></div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
                <input class="form" type="text" placeholder="Name" id="user_name" required>
                <span id="name_span" style="display: block;"></span>
                <input class="form" type="text" placeholder="Subject" id="user_subject" required>
                <span id="sbj_span" style="display: block;"></span>
                <textarea name="" placeholder="Request/Inquery" cols="60" rows="4" id="user_inquery" required></textarea>
                <span id="inq_span" style="display: block;"></span>
                <a class="btn btn-primary form" id="submit-contact" href="javaScript:void(0)">Submit</a>
                <input id="public_path" value="{{url('')}}" hidden>
            </form>
        </div>
        <div class="wrapper">
            <p id="para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, eligendi?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, necessitatibus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, eligendi?Lorem ipsum dolor
                sit amet, consectetur adipisicing elit. Ex, necessitatibus!</p>
        </div>
        <footer>
            <p><span>All rights reserved </span>@ mediasci</p>
        </footer>
    </div>
    <script>
        function initMap() {
            var uluru = {
                lat: 30.023942,
                lng: 31.411961
            };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmwXS5_tVJyVxC1nthya--wvGy0NsIDCo&callback=initMap">
    </script>
    <script src="{{url('js/contact.js')}}"></script>
@stop
