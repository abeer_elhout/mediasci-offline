@extends('adminlte::page')

@section('title', 'Internal Design')
@section('css')
    <link href="/css/ourstyle.css" rel="stylesheet">
@stop
@section('content')
  <!-- <div class="container"> -->
    @if(count($post_data) > 0)
      <h1>Edit Internal Design</h1>
    @else
      <h1>Add Internal Design</h1>
    @endif
    <form class="" action="" method="post" enctype="multipart/form-data" id="add_post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
      <div class="form-group">
          <label class="control-label">Title</label>
              <input type='text'  name="title" class="form-control"  value='@if(count($post_data) > 0) {{$post_data->title}} @endif' placeholder="title">
      </div>
      <div class="form-group">
          <label class="control-label">Prief</label>
              <input type='text'  name="short_desc" class="form-control"  value='@if(count($post_data) > 0) {{$post_data->short_description}} @endif' placeholder="Prief">
      </div>
      <div class="form-group">
          <label class="control-label">Description</label>
          <textarea name="log_desc" class="form-control" rows="8" cols="80" placeholder="description">@if(count($post_data) > 0) {{$post_data->long_description}} @endif</textarea>
      </div>
      <div class="form-group">
        <label class="control-label">upload images</label>
          <input type="file" name="images[]" id='images' value="" multiple>
          <div class="col-lg-12" id="imgsDiv">
            @if(count($img_array) > 0)
              @foreach($img_array as $key => $value)
                <div class="podiv1">
                  <a href="javaScript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                  <img class="postimage1" src="{{url('/uploads/'.$value->media_path)}}" />
                  <input hidden name="img_{{$key+10}}" value="{{$value->id}}" />
                </div>
              @endforeach
            @endif
          </div>
          <div class="form-group">
            <label class="control-label">upload home image</label>
              <input type="file" name="homeimage" id='homeimgbtn' value="">
              <div class="col-lg-12" id="homeimgdiv">
                @if(count($post_data) > 0)
                    <div class="podiv2">
                      <a href="javaScript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                      <img class="postimage2" src="{{url('/uploads/'.$post_data->home_img_path->media_path)}}" />
                      <input hidden name="homeimg" value="{{$post_data->home_img_path->id}}" />
                    </div>
                @endif
              </div>
          </div>
      </div>
      <div class="form-group">
          <button type="submit" class="btn btn-primary">Save</button>
      </div>
      <input name="url_type" value="{{$url_type}}" hidden>
      <input id="public_path" value="{{url('')}}" hidden>
    </form>
  <!-- </div> -->

<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="/js/upload_image.js"></script>
@stop
