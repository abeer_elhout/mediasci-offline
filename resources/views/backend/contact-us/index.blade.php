@extends('adminlte::page')

@section('title', 'Contact US Leads')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
      <div class="table-responsive">
        <table class="table table-hover">
          <!-- table-bordered  -->
          <thead>
            <tr>
              <th>Name</th>
              <th>Subject</th>
              <th>Inquery</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count($all_contacts) > 0)
              @foreach($all_contacts as $key => $value)
                <tr>
                  <td><?php echo $value->name; ?></td>
                  <td><?php echo $value->subject; ?></td>
                  <td><?php echo $value->inquery;  ?></td>
                  <td>
                    <a href="{{url('/admin/contact/'.$value->id.'/delete')}}" onclick="return confirm('Are you sure you want to delete this contact')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td style="text-align: center;" colspan="6">Sorry,No Contacts Leads</td>
              </tr>
           @endif
          </tbody>
        </table>
@stop
