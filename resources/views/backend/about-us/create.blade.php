@extends('adminlte::page')

@section('title', 'About Us Page')

@section('content')
  <!-- <div class="container"> -->
    @if(count($about_data) > 0)
      <h1>edit About Us Page</h1>
    @else
      <h1>Add About Us Page</h1>
    @endif
    <form class="" action="" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
      <div class="form-group">
          <label class="control-label">Prief</label>
              <input type='text'  name="short_desc" class="form-control"  value='@if(count($about_data) > 0) {{$about_data->prief}} @endif' placeholder="Prief">
      </div>
      <div class="form-group">
          <label class="control-label">Description</label>
          <textarea name="log_desc" class="form-control" rows="8" cols="80" placeholder="description">@if(count($about_data) > 0) {{$about_data->description}} @endif</textarea>
      </div>
      <div class="form-group">
          <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
  <!-- </div> -->

<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="/js/upload_image.js"></script>
@stop
