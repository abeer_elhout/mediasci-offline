@extends('adminlte::page')

@section('title', 'Contact US Leads')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
      @if (count($about_data) == 0)
        <div class="form-group col-12">
          <a href="{{url('/admin/about-us/create')}}" class="btn btn-primary">Create About Page</a>
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-hover">
          <!-- table-bordered  -->
          <thead>
            <tr>
              <th>Name</th>
              <th>Prief</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count($about_data) > 0)
                <tr>
                  <td><?php echo $about_data->name; ?></td>
                  <td><?php echo $about_data->prief; ?></td>
                  <td><?php echo $about_data->description; ?></td>
                  <td>
                    <a href="{{url('/admin/about-us/edit/'.$about_data->id)}}" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
                    <a href="{{url('/admin/about-us/'.$about_data->id.'/delete')}}" onclick="return confirm('Are you sure you want to delete this page')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
            @else
              <tr>
                <td style="text-align: center;" colspan="6">Sorry,No About US Data</td>
              </tr>
           @endif
          </tbody>
        </table>
@stop
