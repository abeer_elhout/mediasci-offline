@extends('adminlte::page')

@section('title', 'Concept Development')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
      @if (count($posts) == 0)
        <div class="form-group col-12">
      		<a href="{{url('/admin/concept-development/create')}}" class="btn btn-primary">Create concept-development</a>
      	</div>
      @endif
      <div class="table-responsive">
        <table class="table table-hover">
          <!-- table-bordered  -->
          <thead>
            <tr>
              <th>Title</th>
              <th> Prief</th>
              <th> Description</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count($posts) > 0)
                <tr>
                  <td><?php echo $posts->title; ?></td>
                  <td><?php echo $posts->short_description; ?></td>
                  <td><?php echo $posts->long_description;  ?></td>
                  <td>
                    <a href="{{url('/admin/concept-development/edit/'.$posts->id)}}" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
                    <!-- <a href="{{url('/admin/post/'.$posts->id.'/delete')}}" onclick="return confirm('Are you sure you want to delete this concept development')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a> -->
                  </td>
                </tr>
            @else
              <tr>
                <td style="text-align: center;" colspan="6">Sorry,No Concept Development</td>
              </tr>
           @endif
          </tbody>
        </table>
@stop
