$(function(){

  $(document).on('click','#submit-contact',function(){
    var name = $('#user_name').val();
    var sbj = $('#user_subject').val();
    var inq = $('#user_inquery').val();
    if (name==null || name=="")
   {
     $('#name_span').addClass('alert alert-danger');
     $('#name_span').text('Name is Required');
     return false;
   }
   if (sbj==null || sbj=="") {
     $('#sbj_span').addClass('alert alert-danger');
     $('#sbj_span').text('Subject is Required');
     return false;
   }
   if (inq==null || inq=="") {
     $('#inq_span').addClass('alert alert-danger');
     $('#inq_span').text('Inquery is Required');
     return false;
   }
    var ajax_url = $('#public_path').val()+'/save-contact';
    var form_data = {
      name:$('#user_name').val(),
      subject:$('#user_subject').val(),
      inquery:$('#user_inquery').val(),
      _token:$("#csrf_token").val()
    };
    $.ajax({
          url:ajax_url,
          data:form_data,
          dataType:'json',
          type:'post',
          success:function(response){
            console.log(response['responseText']);
            $('#successDiv').append('<strong>Success!</strong> '+response['responseText'])
            document.getElementById('contact-form').reset();
            $('#name_span').hide();
            $('#sbj_span').hide();
            $('#inq_span').hide();
            $('#successDiv').show();
            setTimeout(function() {
              $('#successDiv').hide();
            }, 10000);
          },
        });
        /** end oof ajax **/
  });

  /*** end of onload function ****/
});
