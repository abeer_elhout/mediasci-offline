$(function(){

/*Add images*/
$("#images").change(function(){
  var _token=$("#csrf_token").val();
  var images=$("#images").prop('files');
  var ajax_url = $('#public_path').val()+'/admin/upload';
  $.each(images,function(key,value){
    var formdata=new FormData;
    formdata.append('_token',_token);
    formdata.append('image',images[key]);
    $.ajax({
          url:ajax_url,
          data:formdata,
          dataType:'json',
          async:false,
          type:'post',
          processData: false,
          contentType: false,
          success:function(response){
            console.log(response);
            $('#imgsDiv').prepend('<div class="podiv"><a href="javaScript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a><img class="postimage" src="'+$("#public_path").val()+'/uploads/'+response['file']+'" /><input hidden name="img_'+key+'" value="'+response['id']+'" /></div>');
          },
        });
      });


 });
/*end of Add images*/

$(document).on('click','.podiv1 a',function(){
  $(this).parent().remove();
});


/**************** home image upload ********/
$("#homeimgbtn").change(function(){
  var _token=$("#csrf_token").val();
  var imageName=$("#homeimgbtn").prop('files')[0];
  var ajax_url = $('#public_path').val()+'/admin/upload';
    var formdata=new FormData;
    formdata.append('_token',_token);
    formdata.append('image',imageName);
    $.ajax({
          url:ajax_url,
          data:formdata,
          dataType:'json',
          async:false,
          type:'post',
          processData: false,
          contentType: false,
          success:function(response){
            console.log(response);
            $('#homeimgdiv').prepend('<div class="podiv2"><a href="javaScript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a><img class="postimage2" src="'+$("#public_path").val()+'/uploads/'+response['file']+'" /><input hidden name="homeimg" value="'+response['id']+'" /></div>');
          },
        });


 });
 $(document).on('click','.podiv2 a',function(){
   $(this).parent().remove();
 });



  // end of onload
});
