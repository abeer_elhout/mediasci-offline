<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 150);
            $table->text('short_description');
            $table->text('long_description');
            $table->string('type', 100);
            $table->integer('media_id')->unsigned();
            $table->integer('home_image_id')->unsigned();
            $table->foreign('media_id')->references('id')->on('gallery')->onDelete('cascade');
            $table->foreign('home_image_id')->references('id')->on('gallery')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
