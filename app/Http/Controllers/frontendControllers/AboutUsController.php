<?php

namespace App\Http\Controllers\frontendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutUs;
use App\Models\Posts;
use Request as rquest;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $about_data = AboutUs::first();
      $all_posts = Posts::all();
      return view('front/about')->with('about_data',$about_data)->with('posts',$all_posts);
    }

}
