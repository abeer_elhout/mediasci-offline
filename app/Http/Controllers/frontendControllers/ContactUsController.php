<?php

namespace App\Http\Controllers\frontendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs as contact;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('front/contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all_data = $request->all();
        $conact_data = new contact;
        $conact_data->name = $all_data['name'];
        $conact_data->subject = $all_data['subject'];
        $conact_data->inquery = $all_data['inquery'];
        $conact_data->save();
        return response()->json(['responseText' => 'Your Request Sent Successfully!'], 200);
    }


}
