<?php

namespace App\Http\Controllers\frontendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Media;
use Request as rquest;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /***
     post types [ 'exhibitions','internal-designing','concept-development','branding']
     ***/
    public function index()
    {
      $all_posts = Posts::all();
      return view('front/index')->with('posts',$all_posts);
    }

    public function get_post(Request $request){
      $post_data = [];
      if(rquest::is('exhibitions')){
        $post_data = Posts::where('type','exhibitions')->first();
      }elseif (rquest::is('internal-designing')) {
        $post_data = Posts::where('type','internal-designing')->first();
      }elseif (rquest::is('concept-development')) {
        $post_data = Posts::where('type','concept-development')->first();
      }elseif (rquest::is('branding')) {
        $post_data = Posts::where('type','branding')->first();
      }
      $img_array = [];
      foreach (unserialize($post_data->media_id) as $key => $value) {
        $img_array[$key] = Media::findOrFail($value);
      }
      return view('front/internal-designing')->with('post_data',$post_data)->with('img_array',$img_array);
    }


}
