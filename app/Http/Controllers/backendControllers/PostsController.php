<?php

namespace App\Http\Controllers\backendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Media;
use Request as rquest;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /***
     post types [ 'exhibitions','internal-designing','concept-development','branding']
     ***/
    public function index()
    {
      $url_type = '';
      if (rquest::is('admin/exhibitions')) {
        $url_type = 'exhibitions';
      }elseif (rquest::is('admin/internal-designing')) {
        $url_type = 'internal-designing';
      }elseif (rquest::is('admin/concept-development')){
        $url_type = 'concept-development';
      }elseif (rquest::is('admin/branding')){
        $url_type = 'branding';
      }
      $all_posts = Posts::where('type',$url_type)->first();
      return view('backend/'.$url_type.'/index')->with('posts',$all_posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      if ($request->isMethod('post')) {
          $post_data = $request->all();
          unset($post_data['images']);
          $images_array = [];
          foreach ($post_data as $key => $value) {
            if (strpos($key,'img_') !== false) {
                array_push($images_array,$value);
            }
          }
          $new_post = new Posts;
          $new_post->title = $post_data['title'];
          $new_post->short_description = $post_data['short_desc'];
          $new_post->long_description = $post_data['log_desc'];
          $new_post->type = $post_data['url_type'];
          $new_post->home_image_id = $post_data['homeimg'];
          $new_post->media_id = serialize($images_array);
          $new_post->save();
          $url_type = '';
          if (rquest::is('admin/exhibitions/create')) {
            $url_type = 'exhibitions';
          }elseif (rquest::is('admin/internal-designing/create')) {
            $url_type = 'internal-designing';
          }elseif (rquest::is('admin/concept-development/create')){
            $url_type = 'concept-development';
          }elseif (rquest::is('admin/branding/create')){
            $url_type = 'branding';
          }
          return redirect('admin/'.$url_type);
      }else {
        $url_type = '';
        if (rquest::is('admin/exhibitions/create')) {
          $url_type = 'exhibitions';
        }elseif (rquest::is('admin/internal-designing/create')) {
          $url_type = 'internal-designing';
        }elseif (rquest::is('admin/concept-development/create')){
          $url_type = 'concept-development';
        }elseif (rquest::is('admin/branding/create')){
          $url_type = 'branding';
        }
        $post_data = [];
        $img_array = [];
        return view('backend/'.$url_type.'/create')->with('url_type',$url_type)
        ->with('post_data',$post_data)->with('img_array',$img_array);
      }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
      if ($request->isMethod('post')) {
        $url_type = '';
        if (rquest::is('admin/exhibitions/edit/*')) {
          $url_type = 'exhibitions';
        }elseif (rquest::is('admin/internal-designing/edit/*')) {
          $url_type = 'internal-designing';
        }elseif (rquest::is('admin/concept-development/edit/*')){
          $url_type = 'concept-development';
        }elseif (rquest::is('admin/branding/edit/*')){
          $url_type = 'branding';
        }
          $post_data = $request->all();
          unset($post_data['images']);
          $images_array = [];
          foreach ($post_data as $key => $value) {
            if (strpos($key,'img_') !== false) {
                array_push($images_array,$value);
            }
          }
          $new_post = Posts::findOrFail($id);
          $new_post->title = $post_data['title'];
          $new_post->short_description = $post_data['short_desc'];
          $new_post->long_description = $post_data['log_desc'];
          $new_post->type = $post_data['url_type'];
          $new_post->home_image_id = $post_data['homeimg'];
          $new_post->media_id = serialize($images_array);
          $new_post->save();
          return redirect('admin/'.$url_type);
      }
      else {
        $url_type = '';
        if (rquest::is('admin/exhibitions/edit/*')) {
          $url_type = 'exhibitions';
        }elseif (rquest::is('admin/internal-designing/edit/*')) {
          $url_type = 'internal-designing';
        }elseif (rquest::is('admin/concept-development/edit/*')){
          $url_type = 'concept-development';
        }elseif (rquest::is('admin/branding/edit/*')){
          $url_type = 'branding';
        }
        $post_data = Posts::where('type',$url_type)->first();
        $img_array = [];
        foreach (unserialize($post_data->media_id) as $key => $value) {
          $img_array[$key] = Media::findOrFail($value);
        }
        $post_data['home_img_path'] = Media::findOrFail($post_data->home_image_id);
        return view('backend/'.$url_type.'/create')
        ->with('url_type',$url_type)
        ->with('post_data',$post_data)
        ->with('img_array',$img_array);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::findOrFail($id);
        $post->delete();
        return redirect()->back();
    }
}
