<?php

namespace App\Http\Controllers\backendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Media;
use Illuminate\Support\Facades\Input;

class MediaController extends Controller
{
    public function save_images(){
        $destinationPath = 'uploads'; // upload path
        $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
        $fileName = random_int(1, 5000) * microtime() . '.' . $extension; // renameing image
        Input::file('image')->move($destinationPath, $fileName); // uploading file to given path

        $new_image = new Media();
        $new_image['media_path'] = $fileName;
        $new_image->save();
        $data['status'] = 'ok';
        $data['data'] = './' . $destinationPath . '/' . $fileName;
        $data['file'] = $fileName;
        $data['id'] = $new_image->id;
		    return json_encode($data);
    }
}
