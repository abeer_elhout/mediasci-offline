<?php

namespace App\Http\Controllers\backendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutUs;
use Request as rquest;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $about_data = AboutUs::first();
      return view('backend/about-us/index')->with('about_data',$about_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')){
          $about_data = $request->all();
          $about_post = new AboutUs;
          $about_post->name = 'About Us';
          $about_post->prief = $about_data['short_desc'];
          $about_post->description = $about_data['log_desc'];
          $about_post->save();
          return redirect('admin/about-us');
        }else {
          $about_data = [];
          return view('backend/about-us/create')->with('about_data',$about_data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
      if ($request->isMethod('post')){
        $about_data = $request->all();
        $about_post = AboutUs::where('id',$id)->first();
        $about_post->prief = $about_data['short_desc'];
        $about_post->description = $about_data['log_desc'];
        $about_post->save();
        return redirect('admin/about-us');
      }else {
        $about_data = AboutUs::where('id',$id)->first();
        return view('backend/about-us/create')->with('about_data',$about_data);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $about_post = AboutUs::findOrFail($id);
      $about_post->delete();
      return redirect()->back();
    }
}
