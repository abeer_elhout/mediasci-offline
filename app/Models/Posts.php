<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';

    public function media_path(){
      return $this->belongsTo('App\Models\Media','media_id');
    }
}
