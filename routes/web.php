<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','frontendControllers\PostsController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

/********** admin routes ************/
Route::group(['prefix' => 'admin'], function () {
  /*
  ** exhibitions routes
  */
  Route::get('/exhibitions','backendControllers\PostsController@index');
  Route::any('/exhibitions/create','backendControllers\PostsController@create');
  Route::any('/exhibitions/edit/{id}','backendControllers\PostsController@edit');
  /*
  ** internal-designing routes
  */
  Route::get('/internal-designing','backendControllers\PostsController@index');
  Route::any('/internal-designing/create','backendControllers\PostsController@create');
  Route::any('/internal-designing/edit/{id}','backendControllers\PostsController@edit');
  /*
  ** concept-development routes
  */
  Route::get('/concept-development','backendControllers\PostsController@index');
  Route::any('/concept-development/create','backendControllers\PostsController@create');
  Route::any('/concept-development/edit/{id}','backendControllers\PostsController@edit');
  /*
  ** branding routes
  */
  Route::get('/branding','backendControllers\PostsController@index');
  Route::any('/branding/create','backendControllers\PostsController@create');
  Route::any('/branding/edit/{id}','backendControllers\PostsController@edit');
  /***** delete post *****/
  Route::get('/post/{id}/delete','backendControllers\PostsController@destroy');
  /*
  ** about-us routes
  */
  Route::get('/about-us','backendControllers\AboutUsController@index');
  Route::any('/about-us/create','backendControllers\AboutUsController@create');
  Route::any('/about-us/edit/{id}','backendControllers\AboutUsController@edit');
  Route::get('/about-us/{id}/delete','backendControllers\AboutUsController@destroy');
  /*
  ** contact-us routes
  */
  Route::get('/contact-us','backendControllers\ContactUsController@index');
  Route::get('/contact/{id}/delete','backendControllers\ContactUsController@destroy');
  /************ upload image ************/
  Route::post('/upload','backendControllers\MediaController@save_images');
});

/********** front end  routes ************/
Route::get('/index','frontendControllers\PostsController@index');
Route::get('/about-us','frontendControllers\AboutUsController@index');
Route::get('/contact-us','frontendControllers\ContactUsController@index');
Route::get('/exhibitions','frontendControllers\PostsController@get_post');
Route::get('/internal-designing','frontendControllers\PostsController@get_post');
Route::get('/concept-development','frontendControllers\PostsController@get_post');
Route::get('/branding','frontendControllers\PostsController@get_post');
Route::post('/save-contact','frontendControllers\ContactUsController@store');
